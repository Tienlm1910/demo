package com.infoplusvn.trainingach;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.infoplusvn.trainingach.entity.AchBank;
import com.infoplusvn.trainingach.entity.AchBankId;
import com.infoplusvn.trainingach.models.MessageOutbound;
import com.infoplusvn.trainingach.repository.AchBankCustomRepository;
import com.infoplusvn.trainingach.repository.AchBankRepository;
import com.infoplusvn.trainingach.service.AchBankService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@SpringBootTest
class TrainingachApplicationTests {

    @Autowired
    private AchBankRepository achBankRepository;

    @Autowired
    private AchBankCustomRepository achBankCustomRepository;

    @Autowired
    private AchBankService achBankService;


    //@Transactional
    //@Modifying
    @Test
    void contextLoads() throws ClassNotFoundException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
//        AchBankId achBankId = new AchBankId("HVBKVNVN", "970102");
//        AchBank achBank = new AchBank(achBankId, "Vietcombank", "ACH", "1");
//        achBankRepository.save(achBank);

//        log.info(mapper.writeValueAsString(achBankRepository.findAchBankByBankCode("HVBKVNVN")));
//        achBankService.insertBankInfo();

        MessageOutbound res = achBankCustomRepository.getMessageOutbound("970102", "XXXXYYYYYYAAAAABBBBB");
        log.info(mapper.writeValueAsString(res));
//        Class cls = Class.forName("TrainingachApplicationTests");
//
//        // returns the ClassLoader object associated with this Class
//        ClassLoader cLoader = cls.getClassLoader();
//
//        System.out.println(cLoader.getClass());

//        // finds resource
//        URL url = cLoader.getSystemResource("file.txt");
//        System.out.println("Value = " + url);
//
//        // finds resource
//        url = cLoader.getSystemResource("newfolder/a.txt");
//        System.out.println("Value = " + url);

    }

}
