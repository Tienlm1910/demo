package com.infoplusvn.trainingach.service;

import java.text.ParseException;

public interface AchBankService {
    String insertBankInfo();
    String sendMessageOutbound() throws ParseException;
}
