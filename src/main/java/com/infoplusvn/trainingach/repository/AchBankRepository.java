package com.infoplusvn.trainingach.repository;

import com.infoplusvn.trainingach.entity.AchBank;
import com.infoplusvn.trainingach.entity.AchBankId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AchBankRepository extends JpaRepository<AchBank, AchBankId> {

    @Query("select achBank from AchBank achBank where achBank.achBankId.bankCode = :bankCode")
    AchBank findAchBankByBankCode(@Param("bankCode") String bankCode);
}
