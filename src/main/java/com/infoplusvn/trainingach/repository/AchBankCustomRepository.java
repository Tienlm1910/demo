package com.infoplusvn.trainingach.repository;

import com.infoplusvn.trainingach.models.MessageOutbound;

public interface AchBankCustomRepository {

    MessageOutbound getMessageOutbound( String binCode , String senderRefNo );
}
