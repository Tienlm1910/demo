package com.infoplusvn.trainingach.repository;

import com.infoplusvn.trainingach.entity.AchTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface AchTransactionRepository extends JpaRepository<AchTransaction , BigDecimal> {
}
