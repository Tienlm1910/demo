package com.infoplusvn.trainingach.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AchBankId implements Serializable {

    @Column(name = "BANK_CODE")
    private String bankCode;

    @Column(name = "BIN_CODE")
    private String binCode;
}
