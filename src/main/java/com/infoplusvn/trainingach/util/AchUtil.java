package com.infoplusvn.trainingach.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.infoplusvn.trainingach.constant.AppConstant;
import com.infoplusvn.trainingach.entity.AchBank;
import com.infoplusvn.trainingach.entity.AchBankId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AchUtil {

    public static Logger logger = LoggerFactory.getLogger(AchUtil.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private static JsonNode bankJSON = null;
    private static JsonNode outboundJSON = null;

    static {
        try {
            bankJSON = mapper
                    .readTree(ResourceUtils.getFile("classpath:" + AppConstant.JsonConfig.JSON_TEMP_BANK));
            outboundJSON = mapper
                    .readTree(ResourceUtils.getFile("classpath:" + AppConstant.JsonConfig.JSON_TEMP_MESSAGE_OUTBOUND));
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static ArrayNode banks() {
        ArrayNode banks = (ArrayNode) bankJSON.path("banks");
        return banks;
    }

    public static Map<String, List> getActionTyoe() {
        Map<String, List> map = new HashMap<>();
        List<String> create = new ArrayList<>();
        List<String> update = new ArrayList<>();
        for (JsonNode bank : banks()) {
            String actionType = bank.get("actionType").asText();
            String bankCode = bank.get("bankCode").asText();
            if (actionType.equals(AppConstant.BankStatus.CREATE)) {
                create.add(bankCode);
            }
            if (actionType.equals(AppConstant.BankStatus.UPDATE)) {
                update.add(bankCode);
            }
        }
        map.put(AppConstant.BankStatus.CREATE, create);
        map.put(AppConstant.BankStatus.UPDATE, update);
        return map;
    }

    public static ArrayNode bankInfo() {
        ArrayNode bankInfos = mapper.createArrayNode();
        for (JsonNode bank : banks()) {
            ObjectNode bankInfo = (ObjectNode) bank;
            bankInfo.remove("actionType");
            bankInfos.add(bankInfo);
        }
        return bankInfos;
    }

    public static Map<String, ObjectNode> bank() {
        Map<String, ObjectNode> map = new HashMap<>();
        for (JsonNode bank : bankInfo()) {
            ObjectNode bankInfo = (ObjectNode) bank;
            map.put(bank.get("bankCode").asText(), bankInfo);
        }
        return map;
    }

    public static AchBank convertIntoObject(Map<String, ObjectNode> map, String bankCode) {
        AchBank achBank = null;
        if (map.containsKey(bankCode)) {
            ObjectNode bankInfo = map.get(bankCode);
            AchBankId achBankId = new AchBankId(bankInfo.get("bankCode").asText(),
                    bankInfo.get("binCode").asText());
            achBank = new AchBank(achBankId,
                    bankInfo.get("bankName").asText(),
                    bankInfo.get("systemType").asText());

        }
        return achBank;
    }

    public static Map<String ,String> messageOutbound(){
        Map<String , String> map = new HashMap<>();
        JsonNode message = outboundJSON.path("body");
        String binCode = message.get("binCode").asText();
        String senderRefNo = message.get("senderRefNo").asText();
        map.put("binCode" ,binCode);
        map.put("senderRefNo" ,senderRefNo);
        return map;
    }


    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
//        logger.info(bankJSON.toPrettyString());
//        logger.info( bankInfo().toPrettyString());
//        logger.info(String.valueOf(bank().containsKey("HVBKVNVN")));
       // logger.info(bank().toString());
        logger.info(messageOutbound().toString());

        Object achBank = objectMapper.readValue(bank().get("HVBKVNVN").toString(), Object.class);
        // logger.info(achBank.toString());
        //logger.info(getActionTyoe().toString());

       /* try {

            JsonNode root = mapper.readTree(ResourceUtils.getFile("classpath:" + "user.json"));

            String resultOriginal = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
            System.out.println("Before Update " + resultOriginal);

            // 1. Update id to 1000
            ((ObjectNode) root).put("id", 1000L);

            // 2. If middle name is empty , update to M
            ObjectNode nameNode = (ObjectNode) root.path("name");
            if ("".equals(nameNode.path("middle").asText())) {
                nameNode.put("middle", "M");
            }

            // 3. Create a new field in nameNode
            nameNode.put("nickname", "mkyong");

            // 4. Remove last field in nameNode
            nameNode.remove("last");

            // 5. Create a new ObjectNode and add to root
            ObjectNode positionNode = mapper.createObjectNode();
            positionNode.put("name", "Developer");
            positionNode.put("years", 10);
            ((ObjectNode) root).set("position", positionNode);

            // 6. Create a new ArrayNode and add to root
            ArrayNode gamesNode = mapper.createArrayNode();

            ObjectNode game1 = mapper.createObjectNode().objectNode();
            game1.put("name", "Fall Out 4");
            game1.put("price", 49.9);

            ObjectNode game2 = mapper.createObjectNode().objectNode();
            game2.put("name", "Dark Soul 3");
            game2.put("price", 59.9);

            gamesNode.add(game1);
            gamesNode.add(game2);
            ((ObjectNode) root).set("games", gamesNode);

            // 7. Append a new Node to ArrayNode
            ObjectNode email = mapper.createObjectNode();
            email.put("type", "email");
            email.put("ref", "abc@mkyong.com");

            JsonNode contactNode = root.path("contact");
            ((ArrayNode) contactNode).add(email);

            String resultUpdate = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);

            System.out.println("After Update " + resultUpdate);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

}