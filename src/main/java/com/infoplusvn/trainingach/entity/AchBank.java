package com.infoplusvn.trainingach.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "ACH_BANK")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AchBank extends Auditable<Date> {

    @EmbeddedId
    private AchBankId achBankId;

    @Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "SYSTEM_TYPE")
    private String systemType;

    @Column(name = "RECORD_STATUS")
    private boolean recordStatus;

    public AchBank(AchBankId achBankId, String bankName, String systemType) {
        this.achBankId = achBankId;
        this.bankName = bankName;
        this.systemType = systemType;
    }
}
