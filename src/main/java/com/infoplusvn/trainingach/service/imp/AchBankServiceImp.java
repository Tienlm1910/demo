package com.infoplusvn.trainingach.service.imp;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.infoplusvn.trainingach.constant.AppConstant;
import com.infoplusvn.trainingach.entity.AchBank;
import com.infoplusvn.trainingach.entity.AchTransaction;
import com.infoplusvn.trainingach.models.DataObj;
import com.infoplusvn.trainingach.models.MessageOutbound;
import com.infoplusvn.trainingach.repository.AchBankCustomRepository;
import com.infoplusvn.trainingach.repository.AchBankRepository;
import com.infoplusvn.trainingach.repository.AchTransactionRepository;
import com.infoplusvn.trainingach.service.AchBankService;
import com.infoplusvn.trainingach.util.AchUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
public class AchBankServiceImp implements AchBankService {

    @Autowired
    private AchBankRepository achBankRepository;

    @Autowired
    private AchBankCustomRepository achBankCustomRepository;

    @Autowired
    private AchTransactionRepository achTransactionRepository;

    @Transactional
    @Override
    public String insertBankInfo() {
        Map<String, List> bank = AchUtil.getActionTyoe();
        AtomicReference<String> res = new AtomicReference<>("");

        try {
            bank.forEach((k, v) -> {
                if (k.equals("CREATE")) {
                    for (Object bankCode : v) {
                        Optional<AchBank> bankOptional = Optional.ofNullable(
                                achBankRepository.findAchBankByBankCode(bankCode.toString()));
                        if (bankOptional.isPresent()) {
                            res.set("Bank infomation is existed");
                        } else {
                            Map<String, ObjectNode> map = AchUtil.bank();
                            AchBank achBank = AchUtil.convertIntoObject(map, bankCode.toString());
                            achBankRepository.save(achBank);
                            DataObj dataObj = new DataObj("00", "Success", null);
                            res.set("Bank infomation is existed");
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }
        return res.get();
    }


    @Override
    public String sendMessageOutbound() throws ParseException {
        String res = "";
        Map<String, String> map = AchUtil.messageOutbound();
        String binCode = map.get("binCode");
        String senderRefNo = map.get("senderRefNo");
        MessageOutbound messageOutbound =
                achBankCustomRepository.getMessageOutbound(binCode, senderRefNo);
        String outIn = "out";
        String transStatus = "system received";
        String transType = "request";
        String msgContent = "outbound";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Date transDt = AppConstant.covertStringToDate(formatter.format(date));
        AchTransaction achTransaction = new AchTransaction(msgContent, outIn, transType, transStatus, transDt);
        achTransactionRepository.save(achTransaction);
        res = messageOutbound.getRes();

        return res;
    }
}
