package com.infoplusvn.trainingach.controller;

import com.infoplusvn.trainingach.service.AchBankService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;

import java.text.ParseException;

@Controller
@Slf4j
public class AchBankController {

    @Autowired
    private AchBankService achBankService;

    @PutMapping(value = "npgw/api/v1/bank/crud" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createNewBank(){
        String response = achBankService.insertBankInfo();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping(value = "npgw/api/v1/message/out" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> convertBank() throws ParseException {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("globalId",
                "XXXXYYYYYYAAAAABBBBB");
        String res = achBankService.sendMessageOutbound();
        return ResponseEntity.ok().headers(responseHeaders).body(res);
    }
}
