package com.infoplusvn.trainingach.models;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class MessageOutbound {

    private String bankCode;
    private String bankName;
    private String senderRefNo;
    private String res;
}
