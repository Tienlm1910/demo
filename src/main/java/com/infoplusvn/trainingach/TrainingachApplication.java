package com.infoplusvn.trainingach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingachApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainingachApplication.class, args);
    }

}
