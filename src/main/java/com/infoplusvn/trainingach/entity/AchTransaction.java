package com.infoplusvn.trainingach.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Blob;
import java.util.Date;

@Entity
@Table(name = "ACH_TRANSACTION")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AchTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ACH_BANK")
    @SequenceGenerator(sequenceName = "SEQ_ACH_BANK", name = "SEQ_ACH_BANK", allocationSize = 1)
    @Column(name = "TRANS_ID")
    private BigDecimal transId;

    @Column(name = "MSG_CONTENT")
    private String msgContent;

    @Column(name = "OUT_IN")
    private String outIn;

    @Column(name = "TRANS_TYPE")
    private String transType;

    @Column(name = "TRANS_STATUS")
    private String transStatus;

    @Column(name = "TRANS_DT")
    private Date transDt;

    public AchTransaction(String msgContent, String outIn, String transType, String transStatus ,Date transDt) {
        this.msgContent = msgContent;
        this.outIn = outIn;
        this.transType = transType;
        this.transStatus = transStatus;
        this.transDt = transDt;
    }
}
