package com.infoplusvn.trainingach.repository.custom;

import com.infoplusvn.trainingach.models.MessageOutbound;
import com.infoplusvn.trainingach.repository.AchBankCustomRepository;
import org.hibernate.procedure.ProcedureOutputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;


@Repository
public class AchBankRepositoryImp implements AchBankCustomRepository {
    public static Logger logger = LoggerFactory.getLogger(AchBankRepositoryImp.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public MessageOutbound getMessageOutbound(String binCode, String senderRefNo) {
        MessageOutbound messageOutbound = null;
        String res;
        StoredProcedureQuery spQuery = entityManager.createStoredProcedureQuery("P_OUTBOUND_MESSAGE")
                .registerStoredProcedureParameter(1, String.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, String.class, ParameterMode.INOUT)
                .registerStoredProcedureParameter(3, String.class, ParameterMode.OUT)
                .registerStoredProcedureParameter(4, String.class, ParameterMode.OUT)
                .registerStoredProcedureParameter(5, String.class, ParameterMode.OUT)
                .setParameter(1, binCode)
                .setParameter(2, senderRefNo);

        try {
            spQuery.execute();
            String bankCode = (String) spQuery.getOutputParameterValue(3);
            String bankName = (String) spQuery.getOutputParameterValue(4);
            res = (String) spQuery.getOutputParameterValue(5);
            messageOutbound = new MessageOutbound(bankCode, bankName, senderRefNo, res);
        } catch (Exception e) {
            logger.info(e.getMessage());
            res = (String) spQuery.getOutputParameterValue(5);
            String bankCode = "bankCode not found";
            String bankName = "bankName not found";
            messageOutbound = new MessageOutbound(bankCode, bankName, senderRefNo, res);
        } finally {
            spQuery.unwrap(ProcedureOutputs.class)
                    .release();
        }
        return messageOutbound;
    }
}